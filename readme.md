Instructions:
Prerequisites: gradle 3.4.1 should be installed

1) run gradle build
2) run gradle eclipse - this will create an eclipse project
3) create a fresh workspace and import this project
4) Set the correct runtime environment (in our case it was tomcat7) and add it as a library to the build path
5) Run/debug the project
6) If the server is running on port 8080, the project can be seen at: http://localhost:8080/ShopifyPOC/