<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
 
<center>
<h1>Product List</h1>       
<table border="1" >  
<tr><th>Id</th><th>Title</th><th>Vendor</th><th>Date</th></tr>  
   <c:forEach var="product" items="${productlist}">   
   <tr>  
   <td>${product.id}</td>  
   <td>${product.title}</td>  
   <td>${product.vendor}</td>  
   <td>${product.createdAt}</td>  
   </tr>  
   </c:forEach> 
</table>
</center> 