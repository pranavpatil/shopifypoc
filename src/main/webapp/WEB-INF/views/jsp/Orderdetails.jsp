<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
 
<center>
<h1>Order List</h1>       
<table border="1" >  
<tr><th>Id</th><th>Date<th>Customer email</th><th>No. Product</th><th> Total Tax</th><th>Total</th><th>Payment Status</th></tr>  
   <c:forEach var="order" items="${orderlist}">   
   <tr>  
   <td>${order.id}</td>  
   <td>${order.processedAt}</td>  
   <td>${order.email}</td>  
   <td>${order.number}</td>  
   <td>${order.totalTax}</td>  
   <td>${order.totalPrice}</td>
   <td>${order.financialStatus}</td>  
   </tr>  
   </c:forEach>  
</table> 
</center> 