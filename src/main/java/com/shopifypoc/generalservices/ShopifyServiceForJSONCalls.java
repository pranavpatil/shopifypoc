package com.shopifypoc.generalservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.stereotype.Service;



@SuppressWarnings("deprecation")
@Service
public class ShopifyServiceForJSONCalls 
{

	
	/**
	 * 
	 * @param url   Send Request to Shopify.com 
	 * @return
	 */

	public String callShopify(String url) 
	{
		

		try {

			PoolingHttpClientConnectionManager clientConnectionManager = new PoolingHttpClientConnectionManager(getRegistry());
			clientConnectionManager.setMaxTotal(100);
			clientConnectionManager.setDefaultMaxPerRoute(20);
			HttpClient httpClient = HttpClients.custom().setConnectionManager(clientConnectionManager).build();
			HttpGet getRequest = new HttpGet(url);
			
		
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("user-agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader((response.getEntity().getContent())));

			String output;
			StringBuilder finalResponse =new StringBuilder();


			while ((output = br.readLine()) != null) 
			{
				finalResponse.append(output);
			}

			
			return finalResponse.toString();

		} 
		catch (ClientProtocolException e) 
		{

			e.printStackTrace();
			return null;

		} 
		catch (IOException e) 
		{

			e.printStackTrace();
			return null;
		}
	}

/**
 * 
 * @return enable ssl
 */
	
	private  Registry<ConnectionSocketFactory> getRegistry()  
	{
		javax.net.ssl.SSLContext sslContext;
		try {
			sslContext = SSLContexts.custom().build();

			SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
					new String[]{"TLSv1.2"}, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());//getDefaultHostnameVerifier());
			return RegistryBuilder.<ConnectionSocketFactory>create()
					.register("http", PlainConnectionSocketFactory.getSocketFactory())
					.register("https", sslConnectionSocketFactory)
					.build();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	 
	}	
}
