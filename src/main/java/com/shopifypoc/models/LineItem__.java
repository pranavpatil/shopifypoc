package com.shopifypoc.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LineItem__ {

@SerializedName("id")
@Expose
private Long id;
@SerializedName("variant_id")
@Expose
private Long variantId;
@SerializedName("title")
@Expose
private String title;
@SerializedName("quantity")
@Expose
private Integer quantity;
@SerializedName("price")
@Expose
private String price;
@SerializedName("grams")
@Expose
private Integer grams;
@SerializedName("sku")
@Expose
private String sku;
@SerializedName("variant_title")
@Expose
private String variantTitle;
@SerializedName("vendor")
@Expose
private Object vendor;
@SerializedName("fulfillment_service")
@Expose
private String fulfillmentService;
@SerializedName("product_id")
@Expose
private Long productId;
@SerializedName("requires_shipping")
@Expose
private Boolean requiresShipping;
@SerializedName("taxable")
@Expose
private Boolean taxable;
@SerializedName("gift_card")
@Expose
private Boolean giftCard;
@SerializedName("name")
@Expose
private String name;
@SerializedName("variant_inventory_management")
@Expose
private String variantInventoryManagement;
@SerializedName("properties")
@Expose
private List<Property__> properties = null;
@SerializedName("product_exists")
@Expose
private Boolean productExists;
@SerializedName("fulfillable_quantity")
@Expose
private Integer fulfillableQuantity;
@SerializedName("total_discount")
@Expose
private String totalDiscount;
@SerializedName("fulfillment_status")
@Expose
private Object fulfillmentStatus;
@SerializedName("tax_lines")
@Expose
private List<TaxLine___> taxLines = null;

public Long getId() {
return id;
}

public void setId(Long id) {
this.id = id;
}

public Long getVariantId() {
return variantId;
}

public void setVariantId(Long variantId) {
this.variantId = variantId;
}

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public Integer getQuantity() {
return quantity;
}

public void setQuantity(Integer quantity) {
this.quantity = quantity;
}

public String getPrice() {
return price;
}

public void setPrice(String price) {
this.price = price;
}

public Integer getGrams() {
return grams;
}

public void setGrams(Integer grams) {
this.grams = grams;
}

public String getSku() {
return sku;
}

public void setSku(String sku) {
this.sku = sku;
}

public String getVariantTitle() {
return variantTitle;
}

public void setVariantTitle(String variantTitle) {
this.variantTitle = variantTitle;
}

public Object getVendor() {
return vendor;
}

public void setVendor(Object vendor) {
this.vendor = vendor;
}

public String getFulfillmentService() {
return fulfillmentService;
}

public void setFulfillmentService(String fulfillmentService) {
this.fulfillmentService = fulfillmentService;
}

public Long getProductId() {
return productId;
}

public void setProductId(Long productId) {
this.productId = productId;
}

public Boolean getRequiresShipping() {
return requiresShipping;
}

public void setRequiresShipping(Boolean requiresShipping) {
this.requiresShipping = requiresShipping;
}

public Boolean getTaxable() {
return taxable;
}

public void setTaxable(Boolean taxable) {
this.taxable = taxable;
}

public Boolean getGiftCard() {
return giftCard;
}

public void setGiftCard(Boolean giftCard) {
this.giftCard = giftCard;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getVariantInventoryManagement() {
return variantInventoryManagement;
}

public void setVariantInventoryManagement(String variantInventoryManagement) {
this.variantInventoryManagement = variantInventoryManagement;
}

public List<Property__> getProperties() {
return properties;
}

public void setProperties(List<Property__> properties) {
this.properties = properties;
}

public Boolean getProductExists() {
return productExists;
}

public void setProductExists(Boolean productExists) {
this.productExists = productExists;
}

public Integer getFulfillableQuantity() {
return fulfillableQuantity;
}

public void setFulfillableQuantity(Integer fulfillableQuantity) {
this.fulfillableQuantity = fulfillableQuantity;
}

public String getTotalDiscount() {
return totalDiscount;
}

public void setTotalDiscount(String totalDiscount) {
this.totalDiscount = totalDiscount;
}

public Object getFulfillmentStatus() {
return fulfillmentStatus;
}

public void setFulfillmentStatus(Object fulfillmentStatus) {
this.fulfillmentStatus = fulfillmentStatus;
}

public List<TaxLine___> getTaxLines() {
return taxLines;
}

public void setTaxLines(List<TaxLine___> taxLines) {
this.taxLines = taxLines;
}

}
