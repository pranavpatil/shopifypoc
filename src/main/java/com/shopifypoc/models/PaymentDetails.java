package com.shopifypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class  PaymentDetails {

@SerializedName("credit_card_bin")
@Expose
private Object creditCardBin;
@SerializedName("avs_result_code")
@Expose
private Object avsResultCode;
@SerializedName("cvv_result_code")
@Expose
private Object cvvResultCode;
@SerializedName("credit_card_number")
@Expose
private String creditCardNumber;
@SerializedName("credit_card_company")
@Expose
private String creditCardCompany;

public Object getCreditCardBin() {
return creditCardBin;
}

public void setCreditCardBin(Object creditCardBin) {
this.creditCardBin = creditCardBin;
}

public Object getAvsResultCode() {
return avsResultCode;
}

public void setAvsResultCode(Object avsResultCode) {
this.avsResultCode = avsResultCode;
}

public Object getCvvResultCode() {
return cvvResultCode;
}

public void setCvvResultCode(Object cvvResultCode) {
this.cvvResultCode = cvvResultCode;
}

public String getCreditCardNumber() {
return creditCardNumber;
}

public void setCreditCardNumber(String creditCardNumber) {
this.creditCardNumber = creditCardNumber;
}

public String getCreditCardCompany() {
return creditCardCompany;
}

public void setCreditCardCompany(String creditCardCompany) {
this.creditCardCompany = creditCardCompany;
}

}
