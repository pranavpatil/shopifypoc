
package com.shopifypoc.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("products")
    @Expose
    private List<Product_> products = null;

    public List<Product_> getProducts() {
        return products;
    }

    public void setProducts(List<Product_> products) {
        this.products = products;
    }

	@Override
	public String toString() {
		return "Product [products=" + products + "]";
	}
    
    

}
