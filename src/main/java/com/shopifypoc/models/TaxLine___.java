package com.shopifypoc.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxLine___ {

@SerializedName("title")
@Expose
private String title;
@SerializedName("price")
@Expose
private String price;
@SerializedName("rate")
@Expose
private Double rate;

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getPrice() {
return price;
}

public void setPrice(String price) {
this.price = price;
}

public Double getRate() {
return rate;
}

public void setRate(Double rate) {
this.rate = rate;
}

}
