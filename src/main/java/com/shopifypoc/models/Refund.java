package com.shopifypoc.models;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Refund {

@SerializedName("id")
@Expose
private Long id;
@SerializedName("order_id")
@Expose
private Long orderId;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("note")
@Expose
private String note;
@SerializedName("restock")
@Expose
private Boolean restock;
@SerializedName("user_id")
@Expose
private Integer userId;
@SerializedName("processed_at")
@Expose
private String processedAt;
@SerializedName("refund_line_items")
@Expose
private List<RefundLineItem> refundLineItems = null;
@SerializedName("transactions")
@Expose
private List<Transaction> transactions = null;
@SerializedName("order_adjustments")
@Expose
private List<Object> orderAdjustments = null;

public Long getId() {
return id;
}

public void setId(Long id) {
this.id = id;
}

public Long getOrderId() {
return orderId;
}

public void setOrderId(Long orderId) {
this.orderId = orderId;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getNote() {
return note;
}

public void setNote(String note) {
this.note = note;
}

public Boolean getRestock() {
return restock;
}

public void setRestock(Boolean restock) {
this.restock = restock;
}

public Integer getUserId() {
return userId;
}

public void setUserId(Integer userId) {
this.userId = userId;
}

public String getProcessedAt() {
return processedAt;
}

public void setProcessedAt(String processedAt) {
this.processedAt = processedAt;
}

public List<RefundLineItem> getRefundLineItems() {
return refundLineItems;
}

public void setRefundLineItems(List<RefundLineItem> refundLineItems) {
this.refundLineItems = refundLineItems;
}

public List<Transaction> getTransactions() {
return transactions;
}

public void setTransactions(List<Transaction> transactions) {
this.transactions = transactions;
}

public List<Object> getOrderAdjustments() {
return orderAdjustments;
}

public void setOrderAdjustments(List<Object> orderAdjustments) {
this.orderAdjustments = orderAdjustments;
}

}
