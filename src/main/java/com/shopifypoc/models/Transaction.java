package com.shopifypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("order_id")
@Expose
private Integer orderId;
@SerializedName("amount")
@Expose
private String amount;
@SerializedName("kind")
@Expose
private String kind;
@SerializedName("gateway")
@Expose
private String gateway;
@SerializedName("status")
@Expose
private String status;
@SerializedName("message")
@Expose
private Object message;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("test")
@Expose
private Boolean test;
@SerializedName("authorization")
@Expose
private String authorization;
@SerializedName("currency")
@Expose
private String currency;
@SerializedName("location_id")
@Expose
private Object locationId;
@SerializedName("user_id")
@Expose
private Object userId;
@SerializedName("parent_id")
@Expose
private Object parentId;
@SerializedName("device_id")
@Expose
private Object deviceId;
@SerializedName("receipt")
@Expose
private Receipt_ receipt;
@SerializedName("error_code")
@Expose
private Object errorCode;
@SerializedName("source_name")
@Expose
private String sourceName;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getOrderId() {
return orderId;
}

public void setOrderId(Integer orderId) {
this.orderId = orderId;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getKind() {
return kind;
}

public void setKind(String kind) {
this.kind = kind;
}

public String getGateway() {
return gateway;
}

public void setGateway(String gateway) {
this.gateway = gateway;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Object getMessage() {
return message;
}

public void setMessage(Object message) {
this.message = message;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public Boolean getTest() {
return test;
}

public void setTest(Boolean test) {
this.test = test;
}

public String getAuthorization() {
return authorization;
}

public void setAuthorization(String authorization) {
this.authorization = authorization;
}

public String getCurrency() {
return currency;
}

public void setCurrency(String currency) {
this.currency = currency;
}

public Object getLocationId() {
return locationId;
}

public void setLocationId(Object locationId) {
this.locationId = locationId;
}

public Object getUserId() {
return userId;
}

public void setUserId(Object userId) {
this.userId = userId;
}

public Object getParentId() {
return parentId;
}

public void setParentId(Object parentId) {
this.parentId = parentId;
}

public Object getDeviceId() {
return deviceId;
}

public void setDeviceId(Object deviceId) {
this.deviceId = deviceId;
}

public Receipt_ getReceipt() {
return receipt;
}

public void setReceipt(Receipt_ receipt) {
this.receipt = receipt;
}

public Object getErrorCode() {
return errorCode;
}

public void setErrorCode(Object errorCode) {
this.errorCode = errorCode;
}

public String getSourceName() {
return sourceName;
}

public void setSourceName(String sourceName) {
this.sourceName = sourceName;
}

}
