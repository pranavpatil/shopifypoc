package com.shopifypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Receipt {

@SerializedName("testcase")
@Expose
private Boolean testcase;
@SerializedName("authorization")
@Expose
private String authorization;

public Boolean getTestcase() {
return testcase;
}

public void setTestcase(Boolean testcase) {
this.testcase = testcase;
}

public String getAuthorization() {
return authorization;
}

public void setAuthorization(String authorization) {
this.authorization = authorization;
}

}
