package com.shopifypoc.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingLine {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("title")
@Expose
private String title;
@SerializedName("price")
@Expose
private String price;
@SerializedName("code")
@Expose
private String code;
@SerializedName("source")
@Expose
private String source;
@SerializedName("phone")
@Expose
private Object phone;
@SerializedName("requested_fulfillment_service_id")
@Expose
private Object requestedFulfillmentServiceId;
@SerializedName("delivery_category")
@Expose
private Object deliveryCategory;
@SerializedName("carrier_identifier")
@Expose
private Object carrierIdentifier;
@SerializedName("tax_lines")
@Expose
private List<Object> taxLines = null;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getPrice() {
return price;
}

public void setPrice(String price) {
this.price = price;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getSource() {
return source;
}

public void setSource(String source) {
this.source = source;
}

public Object getPhone() {
return phone;
}

public void setPhone(Object phone) {
this.phone = phone;
}

public Object getRequestedFulfillmentServiceId() {
return requestedFulfillmentServiceId;
}

public void setRequestedFulfillmentServiceId(Object requestedFulfillmentServiceId) {
this.requestedFulfillmentServiceId = requestedFulfillmentServiceId;
}

public Object getDeliveryCategory() {
return deliveryCategory;
}

public void setDeliveryCategory(Object deliveryCategory) {
this.deliveryCategory = deliveryCategory;
}

public Object getCarrierIdentifier() {
return carrierIdentifier;
}

public void setCarrierIdentifier(Object carrierIdentifier) {
this.carrierIdentifier = carrierIdentifier;
}

public List<Object> getTaxLines() {
return taxLines;
}

public void setTaxLines(List<Object> taxLines) {
this.taxLines = taxLines;
}

}
