package com.shopifypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefundLineItem {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("quantity")
@Expose
private Integer quantity;
@SerializedName("line_item_id")
@Expose
private Integer lineItemId;
@SerializedName("subtotal")
@Expose
private Double subtotal;
@SerializedName("total_tax")
@Expose
private Double totalTax;
@SerializedName("line_item")
@Expose
private LineItem__ lineItem;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getQuantity() {
return quantity;
}

public void setQuantity(Integer quantity) {
this.quantity = quantity;
}

public Integer getLineItemId() {
return lineItemId;
}

public void setLineItemId(Integer lineItemId) {
this.lineItemId = lineItemId;
}

public Double getSubtotal() {
return subtotal;
}

public void setSubtotal(Double subtotal) {
this.subtotal = subtotal;
}

public Double getTotalTax() {
return totalTax;
}

public void setTotalTax(Double totalTax) {
this.totalTax = totalTax;
}

public LineItem__ getLineItem() {
return lineItem;
}

public void setLineItem(LineItem__ lineItem) {
this.lineItem = lineItem;
}

}
