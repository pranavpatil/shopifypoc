package com.shopifypoc.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientDetails {

@SerializedName("browser_ip")
@Expose
private String browserIp;
@SerializedName("accept_language")
@Expose
private Object acceptLanguage;
@SerializedName("user_agent")
@Expose
private Object userAgent;
@SerializedName("session_hash")
@Expose
private Object sessionHash;
@SerializedName("browser_width")
@Expose
private Object browserWidth;
@SerializedName("browser_height")
@Expose
private Object browserHeight;

public String getBrowserIp() {
return browserIp;
}

public void setBrowserIp(String browserIp) {
this.browserIp = browserIp;
}

public Object getAcceptLanguage() {
return acceptLanguage;
}

public void setAcceptLanguage(Object acceptLanguage) {
this.acceptLanguage = acceptLanguage;
}

public Object getUserAgent() {
return userAgent;
}

public void setUserAgent(Object userAgent) {
this.userAgent = userAgent;
}

public Object getSessionHash() {
return sessionHash;
}

public void setSessionHash(Object sessionHash) {
this.sessionHash = sessionHash;
}

public Object getBrowserWidth() {
return browserWidth;
}

public void setBrowserWidth(Object browserWidth) {
this.browserWidth = browserWidth;
}

public Object getBrowserHeight() {
return browserHeight;
}

public void setBrowserHeight(Object browserHeight) {
this.browserHeight = browserHeight;
}

}
