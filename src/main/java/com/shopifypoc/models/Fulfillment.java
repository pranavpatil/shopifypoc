package com.shopifypoc.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fulfillment {

@SerializedName("id")
@Expose
private Long id;
@SerializedName("order_id")
@Expose
private Long orderId;
@SerializedName("status")
@Expose
private String status;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("service")
@Expose
private String service;
@SerializedName("updated_at")
@Expose
private String updatedAt;
@SerializedName("tracking_company")
@Expose
private Object trackingCompany;
@SerializedName("shipment_status")
@Expose
private Object shipmentStatus;
@SerializedName("tracking_number")
@Expose
private String trackingNumber;
@SerializedName("tracking_numbers")
@Expose
private List<String> trackingNumbers = null;
@SerializedName("tracking_url")
@Expose
private String trackingUrl;
@SerializedName("tracking_urls")
@Expose
private List<String> trackingUrls = null;
@SerializedName("receipt")
@Expose
private Receipt receipt;
@SerializedName("line_items")
@Expose
private List<LineItem_> lineItems = null;

public Long getId() {
return id;
}

public void setId(Long id) {
this.id = id;
}

public Long getOrderId() {
return orderId;
}

public void setOrderId(Long orderId) {
this.orderId = orderId;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getService() {
return service;
}

public void setService(String service) {
this.service = service;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public Object getTrackingCompany() {
return trackingCompany;
}

public void setTrackingCompany(Object trackingCompany) {
this.trackingCompany = trackingCompany;
}

public Object getShipmentStatus() {
return shipmentStatus;
}

public void setShipmentStatus(Object shipmentStatus) {
this.shipmentStatus = shipmentStatus;
}

public String getTrackingNumber() {
return trackingNumber;
}

public void setTrackingNumber(String trackingNumber) {
this.trackingNumber = trackingNumber;
}

public List<String> getTrackingNumbers() {
return trackingNumbers;
}

public void setTrackingNumbers(List<String> trackingNumbers) {
this.trackingNumbers = trackingNumbers;
}

public String getTrackingUrl() {
return trackingUrl;
}

public void setTrackingUrl(String trackingUrl) {
this.trackingUrl = trackingUrl;
}

public List<String> getTrackingUrls() {
return trackingUrls;
}

public void setTrackingUrls(List<String> trackingUrls) {
this.trackingUrls = trackingUrls;
}

public Receipt getReceipt() {
return receipt;
}

public void setReceipt(Receipt receipt) {
this.receipt = receipt;
}

public List<LineItem_> getLineItems() {
return lineItems;
}

public void setLineItems(List<LineItem_> lineItems) {
this.lineItems = lineItems;
}

}
