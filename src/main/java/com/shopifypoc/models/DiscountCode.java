package com.shopifypoc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscountCode {

@SerializedName("code")
@Expose
private String code;
@SerializedName("amount")
@Expose
private String amount;
@SerializedName("type")
@Expose
private String type;

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

}
