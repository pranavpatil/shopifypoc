package com.shopifypoc.controller;  
import java.lang.reflect.Type;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;  
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shopifypoc.generalservices.ShopifyServiceForJSONCalls;
import com.shopifypoc.models.Order;
import com.shopifypoc.models.Orders;
import com.shopifypoc.models.Product;
import com.shopifypoc.models.Product_;

@Controller  
public class ShopifyGetAllProduct {  

	private  ShopifyServiceForJSONCalls Sendrequesttoshopify;
	
	@Value("${producturl}")
	String producturl;

	@Value("${orderurl}")
	String orderurl;



	@Autowired
	public ShopifyGetAllProduct(ShopifyServiceForJSONCalls Sendrequesttoshopify) {
		this.Sendrequesttoshopify=Sendrequesttoshopify;
	}


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {

		return  new ModelAndView("index1", "storename", "Myshopify");
	}


	/**
	 * 
	 * @return  Get JSON response  Product Details from shopify and send to Productdetails.jsp
	 */

	@RequestMapping(value = "/getproduct", method = RequestMethod.GET)
	public ModelAndView  Getproduct() {
		String json;
		String serverResponse = Sendrequesttoshopify.callShopify(producturl);
		json = serverResponse;  
		Gson gson = new Gson(); 
		Type objectType = new TypeToken<Product>(){}.getType();
		Product productlist= gson.fromJson(json, objectType);
		List<Product_> product=productlist.getProducts();
		return new ModelAndView("Productdetails", "productlist", product);  


	}  


	/**
	 * 
	 * @return   Get JSON response Order Details from shopify and send to Orderdetails.jsp
	 */

	
	@RequestMapping(value = "/getorder", method = RequestMethod.GET)
	public ModelAndView GetOrder() {

		String json;
		String serverResponse = Sendrequesttoshopify.callShopify(orderurl);
		json = serverResponse; 
		Gson gson = new Gson(); 
		Type objectType = new TypeToken<Orders>(){}.getType();
		Orders orderlist= gson.fromJson(json, objectType);

		List<Order> order=orderlist.getOrders();

		return new ModelAndView("Orderdetails","orderlist",order);  


	}  

}